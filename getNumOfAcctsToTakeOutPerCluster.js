const getNumOfAcctsToTakeOutPerCluster = ({ totalNumOfAcctsToTakeOut, numOfAcctsPerCluster, numOfSameDayPTPAcctsPerCluster, numOfMissedPTPAcctsPerCluster } = {}) => {

  // EXCEPTION: invalid values (negative, string, non-array)
  // EXCEPTION: number of accounts to take out or num of PTP accounts more than total num of accounts (just return all accounts)
	// EXCEPTION: all arrays don't have the same length
	// EXCEPTION: array elements don't correspond to the other arrays

	const totalNumOfPTPacctsPerCluster = numOfAcctsPerCluster.map((a, i) => numOfSameDayPTPAcctsPerCluster[i] + numOfMissedPTPAcctsPerCluster[i]);
	const numOfRemainingAcctsPerCluster = numOfAcctsPerCluster.map((a, i) => a - totalNumOfPTPacctsPerCluster[i]);
	const takeOutPriorityArray = [
		numOfSameDayPTPAcctsPerCluster,
		numOfMissedPTPAcctsPerCluster,
		numOfRemainingAcctsPerCluster
	];

	let count = totalNumOfAcctsToTakeOut;
	let numOfAcctsToTakeOutPerCluster = new Array(numOfAcctsPerCluster.length).fill(0);
	let allPTPHandled = false;

	for (let i = 0; i < takeOutPriorityArray.length && count > 0; i++) {
		let arr = takeOutPriorityArray[i];
		
		// all the accts contained in array below the index of 2 are PTP accounts, which require sequential prioritisation
		if (i < 2) {
			for (let r = 0; r < arr.length && count > 0; r++) {
				const numOfAcctsToTakeOut = Math.min(count, arr[r]);
				count -= numOfAcctsToTakeOut;
				numOfAcctsToTakeOutPerCluster[r] += numOfAcctsToTakeOut;
			}
		} else { // non-PTP accounts require non-sequential even distribution instead
			allPTPHandled = true;
			const numOfAcctsToTakeOutFromEachCluster = Math.floor(count / numOfAcctsPerCluster.length);
			let remainder = count % numOfAcctsPerCluster.length;

			// add the evenly distributed accounts to each cluster 
			for (let r = 0; r < arr.length && count > 0; r++) { 
				const numOfAcctsToTakeOut = Math.min(numOfAcctsToTakeOutFromEachCluster, arr[r]);
				count -= numOfAcctsToTakeOut;
				numOfAcctsToTakeOutPerCluster[r] += numOfAcctsToTakeOut;
			}

			// evenly distribute any remainders from dividing the slots earlier
			for (let r = 0; r < arr.length && count > 0 && remainder > 0; r++) {
				// only add remainder accounts if not all accounts in cluster are taken out
				if (numOfAcctsPerCluster[r] - numOfAcctsToTakeOutPerCluster[r] !== 0) {
					// to equally distribute remainder, we add 1 remainder account to each cluster until the remainder quota is filled up
					remainder -= 1;
					count -= 1;
					numOfAcctsToTakeOutPerCluster[r] += 1;
				}
			}
		}
  }

	return {
		allPTPHandled: numOfAcctsToTakeOutPerCluster.every((a, i) => a - totalNumOfPTPacctsPerCluster[i] >= 0),
		numOfAcctsToTakeOutPerCluster
	}
};

// MAIN

// TEST CASES
// ====================
const testCases = [
	{
		testCase: '✔️ SAME DAY PTP / ✔️ MISSED PTP / ✔️ OTHERS',
		totalNumOfAcctsToTakeOut: 20,
		numOfAcctsPerCluster: [32,12,19,25],
		numOfSameDayPTPAcctsPerCluster: [4,3,1,5],
		numOfMissedPTPAcctsPerCluster: [1,0,1,2],
	},
	{
		testCase: '✔️ SAME DAY PTP / ✔️ MISSED PTP / ❌ OTHERS',
		totalNumOfAcctsToTakeOut: 20,
		numOfAcctsPerCluster: [5,6,2,7],
		numOfSameDayPTPAcctsPerCluster: [4,3,1,5],
		numOfMissedPTPAcctsPerCluster: [1,3,1,2],
	},
	{
		testCase: '❌ SAME DAY PTP / ✔️ MISSED PTP / ✔️ OTHERS',
		totalNumOfAcctsToTakeOut: 20,
		numOfAcctsPerCluster: [15,21,7,18],
		numOfSameDayPTPAcctsPerCluster: [0,0,0,0],
		numOfMissedPTPAcctsPerCluster: [1,3,1,2],
	},
	{
		testCase: '❌ SAME DAY PTP / ❌ MISSED PTP / ✔️ OTHERS',
		totalNumOfAcctsToTakeOut: 20,
		numOfAcctsPerCluster: [15,21,7,18],
		numOfSameDayPTPAcctsPerCluster: [0,0,0,0],
		numOfMissedPTPAcctsPerCluster: [0,0,0,0],
	},
	{
		testCase: '✔️ SAME DAY PTP / ❌ MISSED PTP / ❌ OTHERS',
		totalNumOfAcctsToTakeOut: 20,
		numOfAcctsPerCluster: [4,3,1,5],
		numOfSameDayPTPAcctsPerCluster: [4,3,1,5],
		numOfMissedPTPAcctsPerCluster: [0,0,0,0],
	},
	{
		testCase: '❌ SAME DAY PTP / ❌ MISSED PTP / ❌ OTHERS',
		totalNumOfAcctsToTakeOut: 20,
		numOfAcctsPerCluster: [0,0,0,0],
		numOfSameDayPTPAcctsPerCluster: [0,0,0,0],
		numOfMissedPTPAcctsPerCluster: [0,0,0,0],
	}
];

// // NO PTP
// const testCase = 'NO PTP (Should evenly distribute non-PTP accounts)';
// const totalNumOfAcctsToTakeOut = 29;
// const numOfAcctsPerCluster = [456,131,362,290];
// const numOfSameDayPTPAcctsPerCluster = [0,0,0,0];
// const numOfMissedPTPAcctsPerCluster = [0,0,0,0];

// // ACCOUNTS TO TAKE OUT IS MORE THAN NO. OF PTP ACCOUNTS (Should evenly distribute non-PTP accounts)
// const testCase = 'ACCOUNTS TO TAKE OUT IS MORE THAN NO. OF PTP ACCOUNTS (Should evenly distribute non-PTP accounts)';
// const totalNumOfAcctsToTakeOut = 289;
// const numOfAcctsPerCluster = [456,131,362,290];
// const numOfSameDayPTPAcctsPerCluster = [21,14,38,46];
// const numOfMissedPTPAcctsPerCluster = [9,11,33,28];

// // NO. OF PTP ACCOUNTS IS MORE THAN ACCOUNTS TO TAKE OUT (PTP accounts in first few clusters should be prioristised over later clusters)
// const testCase = 'NO. OF PTP ACCOUNTS IS MORE THAN ACCOUNTS TO TAKE OUT (PTP accounts in first few clusters should be prioristised over later clusters)';
// const totalNumOfAcctsToTakeOut = 34;
// const numOfAcctsPerCluster = [456,131,362,290];
// const numOfSameDayPTPAcctsPerCluster = [78,34,201,107];
// const numOfMissedPTPAcctsPerCluster = [34,55,96,12];

// // MOST PTP ACCOUNTS COME FROM ONE CLUSTER (PTP accounts in first few clusters should be prioristised over later clusters)
// const testCase = 'MOST PTP ACCOUNTS COME FROM ONE CLUSTER (PTP accounts in first few clusters should be prioristised over later clusters)';
// const totalNumOfAcctsToTakeOut = 289;
// const numOfAcctsPerCluster = [456,131,362,290];
// const numOfSameDayPTPAcctsPerCluster = [4,11,198,21];
// const numOfMissedPTPAcctsPerCluster = [2,0,62,12];

// // MOST ACCOUNTS ARE PTPs
// const testCase = 'MOST ACCOUNTS ARE PTPs';
// const totalNumOfAcctsToTakeOut = 20;
// const numOfAcctsPerCluster = [3,2,9,3];
// const numOfSameDayPTPAcctsPerCluster = [1,1,2,1];
// const numOfMissedPTPAcctsPerCluster = [2,0,6,2];

// // MOST ACCOUNTS ARE PTPs
// const testCase = 'MOST ACCOUNTS ARE PTPs';
// const totalNumOfAcctsToTakeOut = 20;
// const numOfAcctsPerCluster = [3,2,9,3];
// const numOfSameDayPTPAcctsPerCluster = [1,1,2,1];
// const numOfMissedPTPAcctsPerCluster = [2,0,6,2];

testCases.forEach(t => {
	const { allPTPHandled, numOfAcctsToTakeOutPerCluster } = getNumOfAcctsToTakeOutPerCluster({
		totalNumOfAcctsToTakeOut: t.totalNumOfAcctsToTakeOut,
		numOfAcctsPerCluster: t.numOfAcctsPerCluster,
		numOfSameDayPTPAcctsPerCluster: t.numOfSameDayPTPAcctsPerCluster,
		numOfMissedPTPAcctsPerCluster: t.numOfMissedPTPAcctsPerCluster,
	});
	
	console.log(t.testCase);
	console.log('===============');
	console.log(`Total number of accounts to take out: ${t.totalNumOfAcctsToTakeOut}`);
	console.log(`Number of accounts per cluster: ${t.numOfAcctsPerCluster}`);
	console.log(`Number of same day PTP accounts per cluster: ${t.numOfSameDayPTPAcctsPerCluster}`);
	console.log(`Number of missed PTP accounts per cluster: ${t.numOfMissedPTPAcctsPerCluster}`);
	console.log('===')
	console.log(`All PTP Handled: ${allPTPHandled}`);
	console.log(`Number of accounts to take out per cluster: ${numOfAcctsToTakeOutPerCluster}`);
	console.log('');
	console.log('---');
	console.log('');
});

// ERROR HANDLING CODE

// if (isNaN(totalNumOfAcctsToTakeOut)) {
// 	throw new Error("Please enter a valid number of accounts to take out");
// }

// if (
// 	!Array.isArray(numOfAcctsPerCluster) ||
// 	!Array.isArray(numOfSameDayPTPAcctsPerCluster) ||
// 	!Array.isArray(numOfMissedPTPAcctsPerCluster)
// ) {
// 	throw new Error("Please enter a valid array of number of accounts/same day PTP accounts/missed PTP accounts per cluster");
// }

// const arraysContainInvalidValue = [
// 	...numOfAcctsPerCluster,
// 	...numOfSameDayPTPAcctsPerCluster,
// 	...numOfMissedPTPAcctsPerCluster
// ].each(e => e > 0 || !isNaN(e))

// if (arraysContainInvalidValue) {
// 	throw new Error("Please enter a valid array of number of accounts/same day PTP accounts/missed PTP accounts per cluster");
// }